import matplotlib.pyplot as plt
import jax
import jax.numpy

# Make datasets.
from notes.data_loader import load_dataset

train = load_dataset("train", is_training=True, batch_size=1000)
train_eval = load_dataset("train", is_training=False, batch_size=10000)
test_eval = load_dataset("test", is_training=False, batch_size=10000)

image1 = next(train)['image']

plt.imshow(image1[0][0])
