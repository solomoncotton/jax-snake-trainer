import jax.numpy as jnp
import jax
from jax import random

key = random.PRNGKey(0)

## TENSORS IN PYTHON

# single array of a certain length L is defined by a tuple (L,)
x = random.normal(key, (4,))
print(x)
# 2d array of rows R and columns C defined by a tuple (R, C)
x = random.normal(key, (3, 1))
print(x)

# Multiply matrices together
print(jnp.dot(x, x.T))


## GRADIENT FUNCTION WITH JAX
# jax.grad automatically differentiates a function
a = jnp.array(3.)
print(a)

b = jnp.array([3., 2., 1.])
print(b)

c = jnp.array([4., 4., 4.])
print(c)

# simple function a * b * c
def f(a, b, c):
    return a * jnp.dot(b, c)

# works as expected
print("f(a, b, c): ", f(a, b, c))

# gradient of f with respect to b should be a*c, which at a, b, c is [12, 12, 12]
print("df/db at a b c: ", jax.grad(f, argnums=1)(a, b, c))

# can also find gradient vector with respect to all params: grad(f) = (d/da, d/db, d/dc)f = (b*c, a*c, a*b)
print("gradient vector at a, b, c: ", jax.grad(f, argnums=(0, 1, 2))(a, b, c))

# by default grad will take the gradient according to the first parameter (i.e. when you don't specify any argnums

## VMAP FUNCTION WITH JAX
# vmap will automatically enable a function to take in a batch of values

# originally function f expected a (3, 1) array as input c
print(c.shape)

# what if we want to run in on 4 separate (3, 1) arrays?
batch_c = jnp.array([[1, 1, 1], [2, 2, 2], [3, 4, 5], [4, 5, 6]])
print("batch_c shape: ", x.shape)

# we can use vmap. in_axes specifies which element to batch, so in this case, (None, None, 0)
# means keep a and b as is in f, and take map over the 0th dimension of the new input to c, x
# since x is a (4, 3) matrix, mapping over the 0th dimension of x gives us the right size vector
#
# this then runs f four times, with c as batch_c[0], batch_c[1], batch_c[2] and batch_c[3]
print("batch f: ", jax.vmap(f, in_axes=(None, None, 0))(a, b, batch_c))

# if instead we had an input array of shape (3, 4), i.e. the transpose of batch_c,
# we would want to tell the axes to map over dimension 1 of this array

print("transpose batch_c", batch_c.T)
print("batch f, transposed c: ", jax.vmap(f, in_axes=(None, None, 1))(a, b, batch_c.T))

# so when we input a batch array of shape (4, 3) we set the map dimension to dimension 0, i.e. the 4
# and when we input a batch array of shape (3, 4) we set the map dimension to 1, i.e. the 4 again

## VMAP CAN BE COMPOSED WITH GRAD

print("batch gradient calculation: ", jax.vmap(jax.grad(f), in_axes=(None, None, 0))(a, b, batch_c))




